# Yii2 Parameters

## Introduction

Allows administration of application parameters in the Yii2 framework.

## Installation

Using [Composer](https://getcomposer.org/)
```sh
$ composer require kfit/yii2-parameters
```
Applying migrations
```sh
$ php yii migrate --migrationPath=@vendor/kfit/yii2-parameters/migrations
```
Config Web.php
```php
'modules'=>[
    'parameters' => [
            'class' => '\kfit\parameters\Module',
            'breadcrumbsBase' => [
                ['label' => 'Base', 'url' => ['/site/base']],
                ['label' => 'Base 2', 'url' => ['/site/base']],
            ],
        'nameSessionEntity' => 'session.entity',
     ],
 ],
```

## Additional Information

In the database once the migrations are executed, the following tables will be created:

| Table name | Description |
| ------ | ------ |
| parameter_categories | This table stores the categories of the parameters. |
| parameters | This table stores the parameter informatión, type of element, options, mandatory, etc. |
| parameter_values | This table stores the default values or the selected value for each entity. |

Data structure for the html_options column in the parameters table: 
```json
{
    "html_options": {},
    "widget_options": {}
}
```
In the **html_options** position, the configuration for the field to be created is stored, for example the popover, the custom classes or the structures to be painted.
On the other hand, the **widget_options** position is used to store the widget configuration used, this only applies to fields that are not type, text or radio.

Data structure for columns with translations:
```json
{
    "default": "example text",
    "language": {
        "es": "Texto de ejemplo",
        "fr": "Exemple de texte"
    }
}
```
As we can see, in the default position we will put the **default** value for the translation and in the **language** position the translations for each language identifier will appear.

### License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause).
Created by: [**KulturFit Developer Team**](http://www.kulturfit.com).