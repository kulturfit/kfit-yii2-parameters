<?php

namespace kfit\parameters\i18n;

use kfit\core\i18n\LabelDbMessageSource as LabelDbMessageSourceCore;

/**
 *
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 */

class LabelDbMessageSource extends LabelDbMessageSourceCore
{ }
