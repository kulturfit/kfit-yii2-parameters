<?php

namespace kfit\parameters\modules\api\controllers;

use kfit\core\rest\ActiveController;
use yii\web\NotFoundHttpException;

/**
 * UserController Clase encargada de presentar y manipular la información del modelo User para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 
 * @copyright (c) 2020, TIC Makers S.A.S
 * @version 0.0.1
 */
class ParameterValuesController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = \kfit\parameters\modules\api\models\base\ParameterValues::class;

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = \kfit\parameters\modules\api\models\searchs\ParameterValues::class;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'parameter_value_id';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        return $actions;
    }

    public function actionView()
    {
        $id = \Yii::$app->request->get('id');
        $code = \Yii::$app->request->get('code');


        if ($id !== null) {
            $item = $id;
            $model = $this->modelClass::findOne($id);
        } else if ($code !== null) {
            $item = $code;
            $model = $this->modelClass::find()
                ->joinWith('parameter as p')
                ->where([
                    $this->modelClass::tableName() . '.' . $this->modelClass::STATUS_COLUMN => $this->modelClass::STATUS_ACTIVE,
                    'p.code' => $code
                ])
                ->one();
        }


        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $item");
        }
    }
}
