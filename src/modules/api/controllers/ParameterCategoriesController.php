<?php

namespace kfit\parameters\modules\api\controllers;

use kfit\core\rest\ActiveController;

/**
 * UserController Clase encargada de presentar y manipular la información del modelo User para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 
 * @copyright (c) 2020, TIC Makers S.A.S
 * @version 0.0.1
 */
class ParameterCategoriesController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = \kfit\parameters\modules\api\models\base\ParameterCategories::class;

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = \kfit\parameters\modules\api\models\searchs\ParameterCategories::class;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'parameter_category_id';
}
