<?php

namespace kfit\parameters\modules\api\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\parameters\models\base\Parameters as ParametersModel;

/**
 * Esta clase representa las búsqueda para el modelo `kfit\parameters\models\base\Parameters`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S. 
 *
 */
class Parameters extends \kfit\parameters\modules\api\models\base\Parameters
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parameter_id', 'parameter_category_id', 'order', 'created_by', 'updated_by'], 'integer'],
            [['code', 'type', 'html_options', 'label', 'help', 'items', 'mandatory', 'transversal', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->setAttributes($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'parameter_id' => $this->parameter_id,
            'parameter_category_id' => $this->parameter_category_id,
            'order' => $this->order,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'html_options', $this->html_options])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'help', $this->help])
            ->andFilterWhere(['like', 'items', $this->items])
            ->andFilterWhere(['like', 'mandatory', $this->mandatory])
            ->andFilterWhere(['like', 'transversal', $this->transversal])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
