<?php

namespace kfit\parameters\modules\api\models\base;

use kfit\parameters\components\Model;
use Yii;

/**
 * Éste es el modelo para la tabla "parameters".
 * System's parameters
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_id Records's unique identifier
 * @property integer $parameter_category_id Parameter's category
 * @property string $code Parameter's code
 * @property string $type Parameter´s type.  Can take the values: select, textarea, radio, date, numeric, text, time, datetime
 * @property string $html_options Html options of the element
 * @property string $label Parameter's label
 * @property string $help Help text for parameter
 * @property integer $order Order for show the element
 * @property string $items Items that can be selected as value
 * @property string $mandatory The parameter is mandatory (Yes or No)
 * @property string $transversal Parameter transversal to the system
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Fecha y hora en que se creó el registro
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property ParameterValues[] $parameterValues Datos relacionados con modelo "ParameterValues"
 * @property ParameterCategories $parameterCategory Datos relacionados con modelo "ParameterCategories"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Parameters extends \kfit\parameters\models\base\Parameters
{ }
