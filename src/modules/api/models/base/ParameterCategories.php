<?php

namespace kfit\parameters\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "parameter_categories".
 * Parameter's categories
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_category_id Records's unique identifier
 * @property string $name Category's name
 * @property string $description Category's description
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Fecha y hora en que se creó el registro
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Parameters[] $parameters Datos relacionados con modelo "Parameters"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ParameterCategories extends \kfit\parameters\models\base\ParameterCategories
{ }
