<?php

namespace kfit\parameters\modules\api;

use Yii;

use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package kfit\yii2-parameters
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [],
        'searchs' => []
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('api')) {
            $app->setModule('api', 'kfit\parameters\modules\api\Module');
        }
        $this->overrideModels($app, 'api');
        Yii::$app->urlManager->addRules($app->getModule('api')->urlManager->rules);
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "kfit\\parameters\\modules\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}
