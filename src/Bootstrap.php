<?php

namespace kfit\parameters;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use kfit\parameters\models\base\ParameterCategories;
use kfit\parameters\models\base\Parameters;
use kfit\parameters\models\base\ParameterValues;
use kfit\parameters\models\searchs\Parameters as ParametersSearch;

/**
 * Class Bootstrap
 *
 * @package kfit\yii2-parameters
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @var array
     */
    private $_modelMap = [
        'base' => [
            'ParameterCategories' => ParameterCategories::class,
            'Parameters' => Parameters::class,
            'ParameterValues' => ParameterValues::class,
        ],
        'searchs' => [
            'Parameters' => ParametersSearch::class,
        ],
        'task' => [],
        'forms' => [],
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('parameters')) {
            $app->setModule('parameters', 'kfit\parameters\Module');
        }

        $this->overrideViews($app, 'parameters');
        $this->overrideModels($app, 'parameters');
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@kfit/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideModels($app, $moduleId)
    {
        /**
         * @var Module $module
         * @var ActiveRecord $modelName
         */
        if ($app->hasModule($moduleId) && ($module = $app->getModule($moduleId)) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $pathModel => $modelMap) {
                foreach ($modelMap as $name => $definition) {
                    $class = "kfit\\{$moduleId}\\models\\{$pathModel}\\" . $name;
                    Yii::$container->set($class, $definition);
                    $modelName = is_array($definition) ? $definition['class'] : $definition;
                    $module->modelMap[$name] = $modelName;
                }
            }
        }
    }
}
