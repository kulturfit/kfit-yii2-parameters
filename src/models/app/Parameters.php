<?php

namespace kfit\parameters\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "parameters".
 * System's parameters
 *
 * @package kfit\parameters\models\app 
 *
 * @property integer $parameter_id Records's unique identifier
 * @property integer $parameter_category_id Parameter's category
 * @property string $code Parameter's code
 * @property string $type Parameter´s type.  Can take the values: select, textarea, radio, date, numeric, text, time, datetime
 * @property string $html_options Html options of the element
 * @property string $label Parameter's label
 * @property string $help Help text for parameter
 * @property integer $order Order for show the element
 * @property string $items Items that can be selected as value
 * @property string $mandatory The parameter is mandatory (Yes or No)
 * @property string $transversal Parameter transversal to the system
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property ParameterValues[] $parameterValues Datos relacionados con modelo "ParameterValues"
 * @property ParameterCategories $parameterCategory Datos relacionados con modelo "ParameterCategories"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Parameters extends \kfit\parameters\models\base\Parameters
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Guardar los datos actualizados del modelo en la tabla parameter_values
     * @param $insert
     * @return boolean
     */
    public function saveValue($data)
    {
        $returnValue = true;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $module = Yii::$app->getModule($this->module->id);
            $entityId = Yii::$app->session->get($module->nameSessionEntity);
            if (is_array($data) && count($data) > 0) {

                foreach ($data as $paramCode => $paramValue) {
                    $modelParameterValue = new ParameterValues();
                    $model = self::find()->where('code ILIKE :code', [':code' => trim($paramCode)])->one();
                    $oldModel = $modelParameterValue::findOne(
                        ['entity_id' => $entityId, 'parameter_id' => $model->parameter_id]
                    );
                    if (!empty($oldModel)) {
                        $modelParameterValue->isNewRecord = false;
                        $oldModel->value = !empty($paramValue) ? $paramValue : '';
                    }
                    if ($model->transversal == 'N') {
                        if (empty($model->parameterValues->entity_id)) {
                            $modelParameterValue->entity_id = $entityId;
                        }
                        if (!$modelParameterValue->isNewRecord) {
                            $modelParameterValue = $oldModel;
                            $modelParameterValue->update(false);
                        } else {
                            $modelParameterValue->parameter_id = $model->parameter_id;
                            $modelParameterValue->value = !empty($paramValue) ? $paramValue : '';
                            $modelParameterValue->save(false);
                        }
                    }
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $returnValue = false;
            throw $e;
        }
        return $returnValue;
    }
}
