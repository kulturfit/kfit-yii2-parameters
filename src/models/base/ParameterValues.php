<?php

namespace kfit\parameters\models\base;

use Yii;
use kfit\parameters\components\Model;

/**
 * Éste es el modelo para la tabla "parameter_values".
 * Parameter's values
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_value_id Records's unique identifier
 * @property integer $parameter_id Parameter to which the value corresponds
 * @property integer $entity_id Entity to which the parameter belongs
 * @property string $value Parameter´s value
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Fecha y hora en que se creó el registro
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Parameters $parameter Datos relacionados con modelo "Parameters"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ParameterValues extends \kfit\parameters\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'parameter_values';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parameter_id', 'value'], 'required'],
            [['parameter_id', 'entity_id'], 'integer'],
            [['value'], 'string'],
            [['parameter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameters::className(), 'targetAttribute' => ['parameter_id' => 'parameter_id']],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'parameter_value_id' => Yii::t($this->module->id, 'Parameter value id'),
            'parameter_id' => Yii::t($this->module->id, 'Parameter'),
            'entity_id' => Yii::t($this->module->id, 'Entity'),
            'value' => Yii::t($this->module->id, 'Value'),
            'active' => Yii::t($this->module->id, 'Active'),
            'created_by' => Yii::t($this->module->id, 'Created By'),
            'created_at' => Yii::t($this->module->id, 'Created At'),
            'updated_by' => Yii::t($this->module->id, 'Updated By'),
            'updated_at' => Yii::t($this->module->id, 'Updated At'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'parameter_value_id' => Yii::t($this->module->id, 'Records\'s unique identifier'),
            'parameter_id' => Yii::t($this->module->id, 'Parameter to which the value corresponds'),
            'entity_id' => Yii::t($this->module->id, 'Entity to which the parameter belongs'),
            'value' => Yii::t($this->module->id, 'Parameter´s value'),
            'active' => Yii::t($this->module->id, 'Indicates whether the record is active or not'),
            'created_by' => Yii::t($this->module->id, 'User\'s id who created the record'),
            'created_at' => Yii::t($this->module->id, 'Fecha y hora en que se creó el registro'),
            'updated_by' => Yii::t($this->module->id, 'Last user\'s id who modified the record'),
            'updated_at' => Yii::t($this->module->id, 'Date and time of the last modification of the record'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Parameter".
     *
     * @return \kfit\parameters\models\base\Parameter
     */
    public function getParameter()
    {
        return $this->hasOne(Parameters::className(), ['parameter_id' => 'parameter_id']);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|ParameterValues[]
     */
    public static function getData($list = true, $attributes = [])
    {
        if (!isset($attributes[self::STATUS_COLUMN])) {
            $attributes[self::STATUS_COLUMN] = self::STATUS_ACTIVE;
        }
        $query = self::find()->where($attributes)->all();
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'parameter_value_id', self::getNameFromRelations());
        }
        return $query;
    }
}
