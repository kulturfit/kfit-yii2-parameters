<?php

namespace kfit\parameters\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "parameters".
 * System's parameters
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_id Records's unique identifier
 * @property integer $parameter_category_id Parameter's category
 * @property string $code Parameter's code
 * @property string $type Parameter´s type.  Can take the values: select, textarea, radio, date, numeric, text, time, datetime
 * @property string $html_options Html options of the element
 * @property string $label Parameter's label
 * @property string $help Help text for parameter
 * @property integer $order Order for show the element
 * @property string $items Items that can be selected as value
 * @property string $mandatory The parameter is mandatory (Yes or No)
 * @property string $transversal Parameter transversal to the system
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property ParameterValues[] $parameterValues Datos relacionados con modelo "ParameterValues"
 * @property ParameterCategories $parameterCategory Datos relacionados con modelo "ParameterCategories"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Parameters extends \kfit\parameters\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'parameters';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parameter_category_id', 'code', 'type', 'label', 'help', 'mandatory', 'transversal'], 'required'],
            [['parameter_category_id', 'order'], 'integer'],
            [['html_options', 'label', 'help', 'items'], 'string'],
            [['code'], 'string', 'max' => 128],
            [['type'], 'string', 'max' => 12],
            [['mandatory', 'transversal'], 'string', 'max' => 1],
            [['parameter_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::$container->get(\app\models\app\ParameterCategories::class), 'targetAttribute' => ['parameter_category_id' => 'parameter_category_id'], 'when' => function ($model) {
                return !($model instanceof \kfit\core\redis\Model);
            }],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => Yii::t('app', 'Código'),
            'parameter_category_id' => Yii::t('app', 'Parameter category'),
            'code' => Yii::t('app', 'Code'),
            'type' => Yii::t('app', 'Type'),
            'html_options' => Yii::t('app', 'Html options'),
            'label' => Yii::t('app', 'Label'),
            'help' => Yii::t('app', 'Help'),
            'order' => Yii::t('app', 'Order'),
            'items' => Yii::t('app', 'Items'),
            'mandatory' => Yii::t('app', 'Mandatory'),
            'transversal' => Yii::t('app', 'Transversal'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'parameter_id' => Yii::t('app', 'Records\'s unique identifier'),
            'parameter_category_id' => Yii::t('app', 'Parameter\'s category'),
            'code' => Yii::t('app', 'Parameter\'s code'),
            'type' => Yii::t('app', 'Parameter´s type.  can take the values: select, textarea, radio, date, numeric, text, time, datetime'),
            'html_options' => Yii::t('app', 'Html options of the element'),
            'label' => Yii::t('app', 'Parameter\'s label'),
            'help' => Yii::t('app', 'Help text for parameter'),
            'order' => Yii::t('app', 'Order for show the element'),
            'items' => Yii::t('app', 'Items that can be selected as value'),
            'mandatory' => Yii::t('app', 'The parameter is mandatory (yes or no)'),
            'transversal' => Yii::t('app', 'Parameter transversal to the system'),
            'active' => Yii::t('app', 'Indicates whether the record is active or not'),
            'created_by' => Yii::t('app', 'User\'s id who created the record'),
            'created_at' => Yii::t('app', 'Date and time the record was created'),
            'updated_by' => Yii::t('app', 'Last user\'s id who modified the record'),
            'updated_at' => Yii::t('app', 'Date and time of the last modification of the record'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "ParameterValues".
     *
     * @return \app\models\app\ParameterValues
     */
    public function getParameterValues()
    {
        $query = $this->hasMany(Yii::$container->get(\app\models\app\ParameterValues::class), ['parameter_id' => 'parameter_id']);
        $query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
        return $query;
    }

    /**
     * Definición de la relación con el modelo "ParameterCategory".
     *
     * @return \app\models\app\ParameterCategory
     */
    public function getParameterCategory()
    {
        return $this->hasOne(Yii::$container->get(\app\models\app\ParameterCategories::class), ['parameter_category_id' => 'parameter_category_id'])->cache(3);
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Parameters[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'parameter_id', static::getNameFromRelations());
        }
        return $query;
    }
}
