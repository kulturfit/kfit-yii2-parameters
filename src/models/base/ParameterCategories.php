<?php

namespace kfit\parameters\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "parameter_categories".
 * 
 *
 * @package parameters
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_category_id Records's unique identifier
 * @property string $name Category's name
 * @property string $description Category's description
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Parameters[] $parameters Datos relacionados con modelo "Parameters"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ParameterCategories extends \kfit\parameters\components\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'parameter_categories';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string'],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'parameter_category_id' => Yii::t('parameters', 'Código'),
            'name' => Yii::t('parameters', 'Name'),
            'description' => Yii::t('parameters', 'Description'),
            'active' => Yii::t('parameters', 'Active'),
            'created_by' => Yii::t('parameters', 'Created by'),
            'created_at' => Yii::t('parameters', 'Created at'),
            'updated_by' => Yii::t('parameters', 'Updated by'),
            'updated_at' => Yii::t('parameters', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'parameter_category_id' => Yii::t('parameters', 'Records\'s unique identifier'),
            'name' => Yii::t('parameters', 'Category\'s name'),
            'description' => Yii::t('parameters', 'Category\'s description'),
            'active' => Yii::t('parameters', 'Indicates whether the record is active or not'),
            'created_by' => Yii::t('parameters', 'User\'s id who created the record'),
            'created_at' => Yii::t('parameters', 'Date and time the record was created'),
            'updated_by' => Yii::t('parameters', 'Last user\'s id who modified the record'),
            'updated_at' => Yii::t('parameters', 'Date and time of the last modification of the record'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "Parameters".
     *
     * @return \parameters\models\parameters\Parameters
     */
    public function getParameters()
    {
        $query = $this->hasMany(Yii::$container->get(\parameters\models\parameters\Parameters::class), ['parameter_category_id' => 'parameter_category_id']);
        $query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
        return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|ParameterCategories[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'parameter_category_id', static::getNameFromRelations());
        }
        return $query;
    }
}
