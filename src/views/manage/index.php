<?php

use kfit\core\widgets\ActiveForm;
use kfit\core\widgets\Tabs;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model kfit\parameters\models\base\Parameters */
/* @var $form kfit\core\widgets\ActiveForm */

$breadcrumbs = $module->breadcrumbsBase;
$this->title = Yii::t($module->id, 'Parameters');
$this->params['breadcrumbs'] = [];
if ($this->context->moduleTitle) {
    $this->moduleTitle = Yii::t('app', $this->context->moduleTitle);
    $this->params['breadcrumbs'][] = $this->moduleTitle;
}
$this->params['breadcrumbs'] = ArrayHelper::merge($this->params['breadcrumbs'], $breadcrumbs);
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card card-tabs">
    <div class="card-body">

        <?php
        $form = ActiveForm::begin([
            'id' => 'parameters-form',
            'method' => 'post'
        ]);
        foreach ($items as $key => $value) {
            $items[$key]['content'] = $this->render('_tab', [
                'data' => $value['data'],
                'model' => $value['model'],
                'form' => $form,
                'module' => $module
            ]);
        }
        ?>

        <div class="parameters-tabs">
            <?= Tabs::widget(['items' => $items]); ?>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="row">
            <div class="col-12">
                <div class="form-group pull-right mt-20">
                    <?= Yii::$app->ui::btnCancel(); ?>
                    <?= Yii::$app->ui::btnSend(null, ['form' => 'parameters-form']); ?>
                </div>
            </div>
        </div>
    </div>
</div>