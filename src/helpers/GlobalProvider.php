<?php

namespace kfit\parameters\helpers;

use Yii;

/**
 * Proveedor de datos por defecto para los elementos de los parámetros
 *
 * @package kfit
 * @subpackage parameters/helpers
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 *
 */
class GlobalProvider
{
}