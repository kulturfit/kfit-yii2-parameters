<?php

namespace kfit\parameters\controllers;

use Yii;
use kfit\core\base\Controller;

/**
* Controlador ParametersController implementa las acciones para el CRUD de el modelo Parameters.
*
* @package kfit\parameters\controllers 
*
* @property string $modelClass Ruta del modelo principal.
* @property string $searchModelClass Ruta del modelo para la búsqueda.
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright Copyright (c) 2020 KulturFit S.A.S. 
* @version 0.0.1
* @since 1.0.0
*/
class ItemsController extends Controller
{
    public $modelClass = \kfit\parameters\models\app\Parameters::class;
    public $searchModelClass = \kfit\parameters\models\searchs\Parameters::class;
}