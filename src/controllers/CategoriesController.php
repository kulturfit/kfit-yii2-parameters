<?php

namespace kfit\parameters\controllers;

use Yii;
use kfit\core\base\Controller;

/**
* Controlador CategoriesController implementa las acciones para el CRUD de el modelo ParameterCategories.
*
* @package kfit\parameters\controllers 
*
* @property string $modelClass Ruta del modelo principal.
* @property string $searchModelClass Ruta del modelo para la búsqueda.
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright Copyright (c) 2020 KulturFit S.A.S. 
* @version 0.0.1
* @since 1.0.0
*/
class CategoriesController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\parameters\models\app\ParameterCategories::class;
    public $searchModelClass = \kfit\parameters\models\searchs\ParameterCategories::class;
}