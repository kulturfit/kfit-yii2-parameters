<?php

namespace kfit\parameters\controllers;

use kfit\core\base\Controller;
use kfit\core\components\DynamicModel;
use Yii;
use yii\helpers\ArrayHelper;
use kfit\parameters\models\app\Parameters;
use kfit\parameters\models\searchs\Parameters as ParametersSearch;

/**
 * Controlador ParametersController implementa las acciones para el CRUD de el modelo Parameters.
 *
 * @package app
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class ManageController extends Controller
{
    public $model = Parameters::class;
    public $searchModel = ParametersSearch::class;

    /**
     * Lista todos registros según el DataProvider.
     *
     * @return string
     */
    public function actionIndex()
    {
        $module = Yii::$app->getModule(Yii::$app->controller->module->id);
        $entityId = Yii::$app->session->get($module->nameSessionEntity);
        if (Yii::$app->request->post()) {
            $values = Yii::$app->request->post('DynamicModel');
            $model = Yii::createObject($this->model);
            if ($model->saveValue($values)) {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, yii::t($module->id, 'it was successfully edited'));
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, yii::t($module->id, 'error when trying to edit'));
            }
            return $this->refresh();
        }

        $sql = "
            select par.parameter_id, par.code, par.type, par.html_options, par.label, par.help, par.order, par.order, par.items, par.mandatory, par.transversal, pca.parameter_category_id, pca.name as category
            from parameters par
            inner join parameter_categories pca ON par.parameter_category_id = pca.parameter_category_id
            where par.active ='Y' AND par.transversal = 'N'
            order by pca.name, par.order
        ";
        $data = Yii::$app->db->createCommand($sql)
            ->queryAll();

        $mapData = ArrayHelper::index($data, 'parameter_id', 'parameter_category_id');
        $mapCategories = ArrayHelper::map($data, 'parameter_category_id', 'category');
        $items = [];

        foreach ($mapCategories as $categoryId => $categoryName) {

            $data = $mapData[$categoryId];

            $attributes = [];
            $attributesLabels = [];
            $rules = [];
            foreach ($data as $key => $parameter) {
                $attribute = strtolower($parameter['code']);
                $attributes[$attribute] = $module->getParameterByCode($parameter['code'], $entityId);
                $data[$key]['attribute'] = $attribute;
                $data[$key]['label'] = Yii::t(Yii::$app->controller->module->id . '-labels', $parameter['label']);
                $attributesLabels[$attribute] = Yii::t(Yii::$app->controller->module->id . '-labels', $data[$key]['label']);
                if ($parameter['mandatory'] == 'Y') {
                    $rules[] = [$data[$key]['attribute'], 'required'];
                }
            }

            $model = new DynamicModel($attributes);
            $model->setAttributeLabels($attributesLabels);
            static::addDynamicRules($rules, $model);

            $items[] = [
                'label' => $categoryName,
                'data' => $data,
                'model' => $model,
                'content' => '',
            ];
        }

        return $this->render(
            'index',
            [
                'items' => $items,
                'module' => $module,
            ]
        );
    }

    /**
     * Añade reglas a un modelo dinamico
     *
     * @param $rulesList
     * @param DynamicModel $modelInstance
     */
    public static function addDynamicRules($rulesList, &$modelInstance)
    {
        foreach ($rulesList as $ruleGroup) {
            $options = isset($ruleGroup[2]) ? $ruleGroup[0] : [];
            $modelInstance->addRule($ruleGroup[0], $ruleGroup[1], $options);
        }
    }
}
