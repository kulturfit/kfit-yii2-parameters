<?php

namespace kfit\parameters\controllers;

use Yii;
use kfit\core\base\Controller;

/**
 * Controlador ParameterValuesController implementa las acciones para el CRUD de el modelo ParameterValues.
 *
 * @package kfit\parameters\controllers 
 *
 * @property string $modelClass Ruta del modelo principal.
 * @property string $searchModelClass Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020  
 *
 */
class ValuesController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\parameters\models\app\ParameterValues::class;
    public $searchModelClass = \kfit\parameters\models\searchs\ParameterValues::class;
}
